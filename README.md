# Shopping CartController

## Instructions
Install dependencies with:
```bash
npm install
```

Start server with:
```bash
npm start
```

Visit [localhost:3000/docs](http://localhost:3000/docs) to view docs and interact with the api endpoints.

