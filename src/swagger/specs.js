const swaggerJSDoc = require('swagger-jsdoc')

const swaggerSpec = swaggerJSDoc({
    failOnErrors: true,
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Express API',
            version: '1.0.0',
        },
    },
    apis: ['src/routes/*.js', `src/swagger/*.yaml`],
})

module.exports = {
    swaggerSpec,
}
