const express = require('express')
const swaggerUi = require('swagger-ui-express')
const routes = require('./routes')
const { swaggerSpec } = require('./swagger/specs')
const errorHandler = require('./middleware/errorHandler')

const app = express()

app.use('/api', routes)
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))
app.use(errorHandler)

module.exports = app
