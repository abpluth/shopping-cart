const { NotFoundError, NotImplementedError } = require('../errors')

const catalog = new Map()

try {
    // eslint-disable-next-line global-require
    const products = require('../products.json')
    products.forEach(product => {
        catalog.set(product.id, product)
    })
} catch (e) {
    console.error('Unable to find products.json')
}

/**
 * Gets a product from catalog by product ID
 * @param {string} id
 * @returns {object}
 * @throws {NotFoundError} Product not found in catalog
 */
function getProductById(id) {
    if (!catalog.has(id)) {
        throw new NotFoundError('Product not found in catalog')
    }

    // TODO: throw error if insufficient qty available for purchase
    return catalog.get(id)
}

/**
 * Updates quantity of product available to purchase
 * @param {string} id
 * @param {number} quantity
 */
function updateQuantityById(id, quantity) {
    catalog.set(id, {
        ...getProductById(id),
        quantity: getProductById(id).quantity + quantity,
    })
}

/**
 * Gets number of items available for purchase (with qty > 0)
 * @returns {number}
 * @throws {NotImplementedError} products.json file not found
 */
function getCatalogSize() {
    if (!catalog.size) {
        throw new NotImplementedError('products.json file not found')
    }
    // this returns the number of products in the catalog that are available for purchase
    return Array.from(catalog.values()).filter(product => product.quantity > 0)
        .length
}

module.exports = {
    getCatalogSize,
    getProductById,
    updateQuantityById,
}
