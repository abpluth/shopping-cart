const { NotFoundError, NotImplementedError } = require('../errors')

class CartController {
    constructor() {
        this.contents = new Map()
        this.totalCost = 0
    }

    /**
     * Checks if cart contains any products
     * @returns {boolean}
     */
    isEmpty() {
        return !this.contents.size
    }

    /**
     * Checks if product exists in cart
     * @param {string} id
     * @returns {boolean}
     */
    hasProductById(id) {
        return this.contents.has(id)
    }

    /**
     * Gets a product object (if it exists) from cart
     * @param {string} id
     * @returns {object}
     * @throws {NotFoundError} Product does not exist in cart
     */
    getProductById(id) {
        if (!this.hasProductById(id)) {
            throw new NotFoundError('Product does not exist in cart')
        }
        return this.contents.get(id)
    }

    /**
     * Gets array of products in cart
     * @returns {object[]} - array of products in cart
     * @throws {NotImplementedError} Cart is empty
     */
    getContents() {
        if (this.isEmpty()) {
            throw new NotImplementedError('Cart is empty')
        }
        return Array.from(this.contents.values())
    }

    /**
     * Adds a product to the cart
     * @param {object} product
     * @throws {NotImplementedError} Product already exists in cart
     */
    addProductToCart(product) {
        if (this.hasProductById(product.id)) {
            throw new NotImplementedError('Product already exists in cart')
        }

        this.contents.set(product.id, product)
        this.totalCost += product.price
    }

    /**
     * Removes a product from the cart
     * @param {string} id
     * @throws {NotFoundError} Product does not exist in cart
     */
    removeProductFromCartById(id) {
        if (!this.hasProductById(id)) {
            throw new NotFoundError('Product does not exist in cart')
        }
        const product = this.contents.get(id)
        this.contents.delete(product.id)
        this.totalCost -= product.price
    }

    /**
     * "Purchases" all the items in the cart
     * @throws {NotImplementedError} Cart is empty
     */
    checkout() {
        if (this.isEmpty()) {
            throw new NotImplementedError('Cart is empty')
        }
        this.contents.clear()
        this.totalCost = 0
    }
}

module.exports = CartController
