const { Router } = require('express')
const catalog = require('../controllers/catalogController')

const routes = Router()

/**
 * @openapi
 * /api/catalog/size:
 *   get:
 *     description: Get size of catalog
 *     tags: [catalog]
 *     responses:
 *       200:
 *         description: success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   default: true
 *                 count:
 *                   type: integer
 *       501:
 *         description: products.json not found
 *         content:
 *           $ref: '#/responses/successFalse'
 */
routes.get('/size', (req, res, next) => {
    try {
        return res.status(200).json({
            success: true,
            count: catalog.getCatalogSize(),
        })
    } catch (e) {
        next(e)
    }
})

/**
 * @openapi
 * /api/catalog/{id}:
 *   get:
 *     description: Get item from catalog
 *     tags: [catalog]
 *     parameters:
 *       - $ref: '#/parameters/id'
 *     responses:
 *       200:
 *         description: success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   default: true
 *                 products:
 *                   $ref: '#/components/schemas/products'
 *       404:
 *         description: item not found in catalog
 *         content:
 *           $ref: '#/responses/successFalse'
 */
routes.get('/:id', (req, res, next) => {
    try {
        return res.status(200).json({
            success: true,
            products: [catalog.getProductById(req.params.id)],
        })
    } catch (e) {
        next(e)
    }
})

module.exports = routes
