const { Router } = require('express')
const Cart = require('./cart')
const Catalog = require('./catalog')

const routes = Router()

routes.use('/cart', Cart)
routes.use('/catalog', Catalog)

module.exports = routes
