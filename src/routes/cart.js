const { Router } = require('express')
const Cart = require('../controllers/cartController')
const catalog = require('../controllers/catalogController')

const routes = Router()
const cart = new Cart()

/**
 * @openapi
 * /api/cart:
 *   get:
 *     description: Gets shopping cart contents
 *     tags: [cart]
 *     responses:
 *       200:
 *         description: success
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   default: true
 *                 totalCost:
 *                   type: integer
 *                 products:
 *                   $ref: '#/components/schemas/products'
 *       501:
 *         description: cart is empty
 *         content:
 *           $ref: '#/responses/successFalse'
 */
routes.get('/', (req, res, next) => {
    try {
        return res.status(200).json({
            success: true,
            totalCost: cart.totalCost,
            products: cart.getContents(),
        })
    } catch (e) {
        next(e)
    }
})

/**
 * @openapi
 * /api/cart/item/{id}:
 *   get:
 *     description: Get item in cart
 *     tags: [cart]
 *     parameters:
 *       - $ref: '#/parameters/id'
 *     responses:
 *       200:
 *         description: fetched item from cart
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   default: true
 *                 products:
 *                   $ref: '#/components/schemas/products'
 *       404:
 *         description: product does not exist in cart
 *         content:
 *           $ref: '#/responses/successFalse'
 */
routes.get('/item/:id', (req, res, next) => {
    try {
        return res.status(200).json({
            success: true,
            products: [cart.getProductById(req.params.id)],
        })
    } catch (e) {
        next(e)
    }
})

/**
 * @openapi
 * /api/cart/item/{id}:
 *   post:
 *     description: Add item to cart
 *     tags: [cart]
 *     parameters:
 *       - $ref: '#/parameters/id'
 *     responses:
 *       200:
 *         description: product added to cart
 *         content:
 *           $ref: '#/responses/successTrue'
 *       404:
 *         description: product does not exist in catalog
 *         content:
 *           $ref: '#/responses/successFalse'
 *       501:
 *         description: product already added to cart
 *         content:
 *           $ref: '#/responses/successFalse'
 */
routes.post('/item/:id', (req, res, next) => {
    try {
        const product = catalog.getProductById(req.params.id)

        cart.addProductToCart(product)
        // decrement catalog qty available for purchase
        catalog.updateQuantityById(req.params.id, -1)
        return res.status(200).json({ success: true })
    } catch (e) {
        next(e)
    }
})

/**
 * @openapi
 * /api/cart/item/{id}:
 *   delete:
 *     description: removes item from cart
 *     tags: [cart]
 *     parameters:
 *       - $ref: '#/parameters/id'
 *     responses:
 *       200:
 *         description: product removed from cart
 *         content:
 *           $ref: '#/responses/successTrue'
 *       404:
 *         description: product does not exist in catalog
 *         content:
 *           $ref: '#/responses/successFalse'
 *       501:
 *         description: product not in cart
 *         content:
 *           $ref: '#/responses/successFalse'
 */
routes.delete('/item/:id', (req, res, next) => {
    try {
        cart.removeProductFromCartById(req.params.id)
        // increment catalog qty available for purchase
        catalog.updateQuantityById(req.params.id, 1)
        return res.status(200).json({ success: true })
    } catch (e) {
        next(e)
    }
})

/**
 * @openapi
 * /api/cart/checkout:
 *   post:
 *     description: Purchase all items in cart
 *     tags: [cart]
 *     responses:
 *       200:
 *         description: success
 *         content:
 *           $ref: '#/responses/successTrue'
 *       501:
 *         description: cart is empty
 *         content:
 *           $ref: '#/responses/successFalse'
 */
routes.post('/checkout', (req, res, next) => {
    try {
        cart.checkout()
        return res.status(200).json({ success: true })
    } catch (e) {
        next(e)
    }
})

module.exports = routes
