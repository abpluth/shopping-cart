class NotImplementedError extends Error {
    constructor(message) {
        super(message)
        this.status = 501
        this.name = 'NotImplementedError'
        this.json = { success: false }
    }
}

module.exports = NotImplementedError
