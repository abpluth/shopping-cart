const NotFoundError = require('./NotFoundError')
const NotImplementedError = require('./NotImplementedError')

module.exports = {
    NotFoundError,
    NotImplementedError,
}
