// eslint-disable-next-line no-unused-vars
const errorHandler = (error, request, response, next) => {
    response.status(error.status).json(error.json)
}

module.exports = errorHandler
