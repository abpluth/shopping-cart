const request = require('supertest')
const app = require('../src/app')
const products = require('../src/products.json')

describe('Catalog', () => {
    describe('GET /catalog/size', () => {
        describe('products.json is found', () => {
            test('Returns catalog size', () =>
                request(app)
                    .get('/api/catalog/size')
                    .expect(200)
                    .expect({ success: true, count: products.length }))
        })

        describe('products.json not found', () => {
            //  TODO: refactor catalogController to allow json file to be specified
            test.skip('Throws not implemented error', () =>
                request(app)
                    .get('/api/catalog/size')
                    .expect(501)
                    .expect({ success: false }))
        })
    })

    describe('GET /api/catalog/:id', () => {
        describe('Product does not exist', () => {
            test('Throws not found error', () =>
                request(app)
                    .get('/api/catalog/99999')
                    .expect(404)
                    .expect({ success: false }))
        })
        describe('Product exists in catalog', () => {
            test('should fail when item id not found', () => {
                const product =
                    products[Math.floor(Math.random() * products.length)]

                return request(app)
                    .get(`/api/catalog/${product.id}`)
                    .expect(200)
                    .expect({ success: true, products: [product] })
            })
        })
    })
})
