const request = require('supertest')
const app = require('../src/app')
const catalog = require('../src/controllers/catalogController')

function getRandomItemFromCatalog() {
    return catalog.getProductById(
        `000${Math.floor(Math.random() * catalog.getCatalogSize())}`.slice(-3),
    )
}
describe('Cart Routes', () => {
    describe('GET /api/cart', () => {
        describe('Cart is empty', () => {
            test('Throws not implemented error', () =>
                request(app)
                    .get('/api/cart')
                    .expect(501)
                    .expect({ success: false }))
        })
        describe('Cart has products', () => {
            // Pick 3 random items from catalog
            const productsInCart = [
                getRandomItemFromCatalog(),
                getRandomItemFromCatalog(),
                getRandomItemFromCatalog(),
            ]
            beforeEach(async () => {
                await Promise.all(
                    productsInCart.map(async product => {
                        await request(app).post(`/api/cart/item/${product.id}`)
                    }),
                )
            })

            test('Returns total and list of items in cart', async () =>
                request(app)
                    .get('/api/cart')
                    .expect(200)
                    .expect({
                        success: true,
                        totalCost: productsInCart.reduce(
                            (sum, product) => sum + product.price,
                            0,
                        ),
                        products: productsInCart,
                    }))
        })
    })

    describe('GET /api/cart/item/:id', () => {
        // Pick random item from catalog
        const product = catalog.getProductById(
            `000${Math.floor(Math.random() * catalog.getCatalogSize())}`.slice(
                -3,
            ),
        )

        describe('Product not in cart', () => {
            test('Throws Not Found Error', () =>
                request(app)
                    .get(`/api/cart/item/${product.id}`)
                    .expect(404)
                    .expect({ success: false }))
        })
        describe('Product is in cart', () => {
            beforeEach(async () => {
                await request(app).post(`/api/cart/item/${product.id}`)
            })

            test('Returns total and list of items in cart', async () =>
                request(app)
                    .get(`/api/cart/item/${product.id}`)
                    .expect(200)
                    .expect({
                        success: true,
                        products: [product],
                    }))
        })
    })

    describe('POST /api/cart/item/:id', () => {
        describe('Product not in catalog', () => {
            test('Throws Not Found Error', () =>
                request(app)
                    .post('/api/cart/item/9999')
                    .expect(404)
                    .expect({ success: false }))
        })
        describe('Product is in catalog', () => {
            const productId = `000${Math.floor(
                Math.random() * catalog.getCatalogSize(),
            )}`.slice(-3)

            afterAll(() =>
                // remove item from cart & make available for purchase
                request(app).delete(`/api/cart/item/${productId}`),
            )

            describe('Product is in stock', () => {
                test('Product added to cart successfully', async () => {
                    await request(app)
                        .post(`/api/cart/item/${productId}`)
                        .expect(200)
                        .expect({ success: true })
                })
            })

            describe('Product already added to cart (out-of-stock)', () => {
                test('Throws not implemented error', async () => {
                    await request(app)
                        .post(`/api/cart/item/${productId}`)
                        .expect(501)
                        .expect({ success: false })
                })
            })
        })
    })

    describe('DELETE /api/cart/item/:id', () => {
        const productId = getRandomItemFromCatalog().id

        describe('Product not in cart', () => {
            test('Throws not found error', () =>
                request(app)
                    .delete(`/api/cart/item/${productId}`)
                    .expect(404)
                    .expect({ success: false }))
        })
        describe('Product is in cart', () => {
            test('Removes product from cart', async () => {
                await request(app)
                    .post(`/api/cart/item/${productId}`)
                    .expect(200)

                return request(app)
                    .delete(`/api/cart/item/${productId}`)
                    .expect(200)
                    .expect({ success: true })
            })
        })
    })

    describe('POST /api/cart/checkout', () => {
        describe('Products in cart', () => {
            // Pick 3 random items from catalog
            const productsInCart = [
                getRandomItemFromCatalog(),
                getRandomItemFromCatalog(),
                getRandomItemFromCatalog(),
            ]

            beforeEach(async () => {
                await Promise.all(
                    productsInCart.map(async product => {
                        await request(app).post(`/api/cart/item/${product.id}`)
                    }),
                )
            })

            test('Returns total and list of items in cart', async () => {
                await request(app)
                    .post(`/api/cart/checkout`)
                    .expect(200)
                    .expect({ success: true })

                // confirm cart is empty
                await request(app).get('/api/cart').expect(501)
                // confirm items not available for purchase
                for (const product of productsInCart) {
                    expect(catalog.getProductById(product.id).quantity).toBe(0)
                }
            })
        })

        describe('No products in cart', () => {
            test('Throws not implemented Error', () =>
                request(app)
                    .post('/api/cart/checkout')
                    .expect(501)
                    .expect({ success: false }))
        })
    })
})
